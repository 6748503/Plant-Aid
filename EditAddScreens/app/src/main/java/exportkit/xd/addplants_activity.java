
	 
	/*
	 *	This content is generated from the API File Info.
	 *	(Alt+Shift+Ctrl+I).
	 *
	 *	@desc 		
	 *	@file 		addplants
	 *	@date 		1623832177301
	 *	@title 		Addplant
	 *	@author 	
	 *	@keywords 	
	 *	@generator 	Export Kit v1.3.figma
	 *
	 */
	

package exportkit.xd;

import android.app.Activity;
import android.os.Bundle;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;

public class addplants_activity extends Activity {

	
	private View _bg__add_plants_;
	private ImageView image_33;
	private ImageView image_13;
	private TextView _12_00;
	private ImageView vector;
	private ImageView vector_ek1;
	private ImageView vector_ek2;
	private ImageView vector_ek3;
	private TextView self_watering_pot;
	private TextView add_plants;
	private TextView jan_pieter;
	private View line_1;
	private View line_2;
	private View line_3;
	private View ellipse_40;
	private TextView jan_pieter_ek1;
	private View rectangle_4;
	private View rectangle_255;
	private ImageView line_8;
	private ImageView line_9;
	private TextView take_photo;
	private View rectangle_10;
	private TextView nickname;
	private TextView plant__species;
	private TextView time;
	private View rectangle_18;
	private View rectangle_257;
	private View _rectangle_256;
	private TextView add_plant;
	private TextView name___;
	private TextView _10_30;
	private TextView watering_frequency;
	private TextView watering_frequency_ek1;
	private View rectangle_258;
	private View rectangle_259;
	private TextView select___;
	private View _rectangle_269;
	private TextView _1_day;
	private View ellipse_221;
	private TextView __;
	private View ellipse_221_ek1;
	private TextView ___ek1;
	private View ellipse_220;
	private TextView ___ek2;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.addplants);

		
		_bg__add_plants_ = (View) findViewById(R.id._bg__add_plants_);
		image_33 = (ImageView) findViewById(R.id.image_33);
		image_13 = (ImageView) findViewById(R.id.image_13);
		_12_00 = (TextView) findViewById(R.id._12_00);
		vector = (ImageView) findViewById(R.id.vector);
		vector_ek1 = (ImageView) findViewById(R.id.vector_ek1);
		vector_ek2 = (ImageView) findViewById(R.id.vector_ek2);
		vector_ek3 = (ImageView) findViewById(R.id.vector_ek3);
		self_watering_pot = (TextView) findViewById(R.id.self_watering_pot);
		add_plants = (TextView) findViewById(R.id.add_plants);
		jan_pieter = (TextView) findViewById(R.id.jan_pieter);
		line_1 = (View) findViewById(R.id.line_1);
		line_2 = (View) findViewById(R.id.line_2);
		line_3 = (View) findViewById(R.id.line_3);
		ellipse_40 = (View) findViewById(R.id.ellipse_40);
		jan_pieter_ek1 = (TextView) findViewById(R.id.jan_pieter_ek1);
		rectangle_4 = (View) findViewById(R.id.rectangle_4);
		rectangle_255 = (View) findViewById(R.id.rectangle_255);
		line_8 = (ImageView) findViewById(R.id.line_8);
		line_9 = (ImageView) findViewById(R.id.line_9);
		take_photo = (TextView) findViewById(R.id.take_photo);
		rectangle_10 = (View) findViewById(R.id.rectangle_10);
		nickname = (TextView) findViewById(R.id.nickname);
		plant__species = (TextView) findViewById(R.id.plant__species);
		time = (TextView) findViewById(R.id.time);
		rectangle_18 = (View) findViewById(R.id.rectangle_18);
		rectangle_257 = (View) findViewById(R.id.rectangle_257);
		_rectangle_256 = (View) findViewById(R.id._rectangle_256);
		add_plant = (TextView) findViewById(R.id.add_plant);
		name___ = (TextView) findViewById(R.id.name___);
		_10_30 = (TextView) findViewById(R.id._10_30);
		watering_frequency = (TextView) findViewById(R.id.watering_frequency);
		watering_frequency_ek1 = (TextView) findViewById(R.id.watering_frequency_ek1);
		rectangle_258 = (View) findViewById(R.id.rectangle_258);
		rectangle_259 = (View) findViewById(R.id.rectangle_259);
		select___ = (TextView) findViewById(R.id.select___);
		_rectangle_269 = (View) findViewById(R.id._rectangle_269);
		_1_day = (TextView) findViewById(R.id._1_day);
		ellipse_221 = (View) findViewById(R.id.ellipse_221);
		__ = (TextView) findViewById(R.id.__);
		ellipse_221_ek1 = (View) findViewById(R.id.ellipse_221_ek1);
		___ek1 = (TextView) findViewById(R.id.___ek1);
		ellipse_220 = (View) findViewById(R.id.ellipse_220);
		___ek2 = (TextView) findViewById(R.id.___ek2);
	
		
		_rectangle_256.setOnClickListener(new View.OnClickListener() {
		
			public void onClick(View v) {
				
				Intent nextScreen = new Intent(getApplicationContext(), my_plants_zonder_menu_activity.class);
				startActivity(nextScreen);
			
		
			}
		});
		
		
		_rectangle_269.setOnClickListener(new View.OnClickListener() {
		
			public void onClick(View v) {
				
				Intent nextScreen = new Intent(getApplicationContext(), add_plants__menu_activity.class);
				startActivity(nextScreen);
			
		
			}
		});
		
		
		//custom code goes here
	
	}
}
	
	